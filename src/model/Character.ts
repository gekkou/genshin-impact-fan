interface Character {
    name: string;
    title: string;
    nation: string;
};

export default Character;